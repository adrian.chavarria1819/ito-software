﻿using Ito.Software.Technical.Test.Business.AutoMapper.CustomerProfile;
using Ito.Software.Technical.Test.Business.AutoMapper.OrderProfile;
using Ito.Software.Technical.Test.Business.AutoMapper.ProductProfile;
using Ito.Software.Technical.Test.Business.Customer;
using Ito.Software.Technical.Test.Business.Customer.Interfaces;
using Ito.Software.Technical.Test.Business.Order;
using Ito.Software.Technical.Test.Business.Order.Interfaces;
using Ito.Software.Technical.Test.Business.Product;
using Ito.Software.Technical.Test.Business.Product.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Ito.Software.Technical.Test.Business
{
    public static class BusinessExtension
    {
        public static void AddBusiness(this IServiceCollection service)
        {
            service.AddAutoMapperBusiness();

            service.AddTransient<ICustomerBusiness, CustomerBusiness>();
            service.AddTransient<IProductBusiness, ProductBusiness>();
            service.AddTransient<IOrderBusiness, OrderBusiness>();
        }


        private static void AddAutoMapperBusiness(this IServiceCollection service)
        {
            service.AddAutoMapper(
                typeof(CustomerProfile),
                typeof(ProductProfile),
                typeof(OrderProfile),
                typeof(OrderItemProfile)
                );
        }
    }
}