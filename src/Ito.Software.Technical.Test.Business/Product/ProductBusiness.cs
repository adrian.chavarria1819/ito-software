﻿using AutoMapper;
using Ito.Software.Technical.Test.Business.Product.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Product;
using Ito.Software.Technical.Test.Domain.Models.Product;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork.Interfaces;

namespace Ito.Software.Technical.Test.Business.Product
{
    public class ProductBusiness : IProductBusiness
    {
        private readonly IUnitOfWorkInfrastructure _unitOfWork;
        private readonly IMapper _mapper;
        public ProductBusiness(IUnitOfWorkInfrastructure unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IList<ProductDto> Get()
        {
            IEnumerable<ProductModel> products = _unitOfWork.ProductRepository.Get();
            return products.Select(_mapper.Map<ProductDto>).ToList();
        }
    }
}
