﻿using Ito.Software.Technical.Test.Domain.Dtos.Product;

namespace Ito.Software.Technical.Test.Business.Product.Interfaces
{
    public interface IProductBusiness
    {
        IList<ProductDto> Get();
    }
}