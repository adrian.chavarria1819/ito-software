﻿using AutoMapper;
using Ito.Software.Technical.Test.Business.Customer.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Ito.Software.Technical.Test.Domain.Models.Customer;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork.Interfaces;

namespace Ito.Software.Technical.Test.Business.Customer
{
    public class CustomerBusiness : ICustomerBusiness
    {
        private readonly IUnitOfWorkInfrastructure _unitOfWork;
        private readonly IMapper _mapper;

        public CustomerBusiness(IUnitOfWorkInfrastructure unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task AddAsync(CustomerDto customer)
        {
            CustomerModel model = _mapper.Map<CustomerModel>(customer);
            await _unitOfWork.CustomerRepository.InsertAsync(model).ConfigureAwait(false);
            await _unitOfWork.SaveAsync().ConfigureAwait(false);
        }

        public IList<CustomerDto> Get()
        {
            IEnumerable<CustomerModel> customers = _unitOfWork.CustomerRepository.Get();
            return customers.Select(_mapper.Map<CustomerDto>).ToList();
        }

    }
}
