﻿using Ito.Software.Technical.Test.Domain.Dtos.Customer;

namespace Ito.Software.Technical.Test.Business.Customer.Interfaces
{
    public interface ICustomerBusiness
    {
        Task AddAsync(CustomerDto customer);
        IList<CustomerDto> Get();
    }
}