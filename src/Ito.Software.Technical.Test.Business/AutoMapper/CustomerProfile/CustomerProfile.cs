﻿using AutoMapper;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Ito.Software.Technical.Test.Domain.Models.Customer;

namespace Ito.Software.Technical.Test.Business.AutoMapper.CustomerProfile
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<CustomerDto, CustomerModel>()
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Name));

            CreateMap<CustomerModel, CustomerDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CustomerName));
        }
    }
}
