﻿using AutoMapper;
using Ito.Software.Technical.Test.Domain.Dtos.Product;
using Ito.Software.Technical.Test.Domain.Models.Product;

namespace Ito.Software.Technical.Test.Business.AutoMapper.ProductProfile
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<ProductModel, ProductDto>()
                .ForMember(dest => dest.Id, src => src.MapFrom(s => s.ProductId))
                .ForMember(dest => dest.Name, src => src.MapFrom(s => s.ProductName))
                .ForMember(dest => dest.UnitPrice, src => src.MapFrom(s => s.UnitPrice))
                ;
        }
    }
}
