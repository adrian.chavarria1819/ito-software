﻿using AutoMapper;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using Ito.Software.Technical.Test.Domain.Models.Order;

namespace Ito.Software.Technical.Test.Business.AutoMapper.OrderProfile
{
    public class OrderItemProfile : Profile
    {
        public OrderItemProfile()
        {
            CreateMap<OrderItemDto, OrderItemModel>();
        }
    }
}
