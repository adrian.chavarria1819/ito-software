﻿using AutoMapper;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using Ito.Software.Technical.Test.Domain.Models.Order;

namespace Ito.Software.Technical.Test.Business.AutoMapper.OrderProfile
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<OrderDto, OrderModel>();
            CreateMap<OrderModel, OrderDto>();
        }
    }
}
