﻿using Ito.Software.Technical.Test.Domain.Dtos.Order;

namespace Ito.Software.Technical.Test.Business.Order.Interfaces
{
    public interface IOrderBusiness
    {
        Task CreateAsync(OrderDto orderDto);
        Task<IList<OrderDto>> GetAsync();
    }
}