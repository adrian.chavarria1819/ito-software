﻿using AutoMapper;
using Ito.Software.Technical.Test.Business.Order.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using Ito.Software.Technical.Test.Domain.Models.Order;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork.Interfaces;

namespace Ito.Software.Technical.Test.Business.Order
{
    public class OrderBusiness : IOrderBusiness
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWorkInfrastructure _unitOfWorkInfrastructure;
        public OrderBusiness(IUnitOfWorkInfrastructure unitOfWorkInfrastructure, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWorkInfrastructure = unitOfWorkInfrastructure;
        }

        public async Task CreateAsync(OrderDto orderDto)
        {
            OrderModel order = _mapper.Map<OrderModel>(orderDto);
            await _unitOfWorkInfrastructure.OrderRepository.InsertAsync(order).ConfigureAwait(false);
            await _unitOfWorkInfrastructure.SaveAsync().ConfigureAwait(false);
        }

        public async Task<IList<OrderDto>> GetAsync()
        {
            IList<OrderModel> orders = _unitOfWorkInfrastructure.OrderRepository.Get().ToList();
            IList<OrderDto> orderDto = orders.Select(_mapper.Map<OrderDto>).ToList();
            return orderDto;
        }

        private IList<OrderItemModel> GetOrderItemModelLists(OrderDto orderDto) =>
            orderDto
            .OrderItems
            .Select(_mapper.Map<OrderItemModel>).ToList();

        private async Task InsertOrderItemRangeAsync(int orderId, IList<OrderItemModel> orderItems)
        {
            foreach (OrderItemModel orderItem in orderItems)
            {
                orderItem.OrderId = orderId;
                await _unitOfWorkInfrastructure.OrderItemRepository.InsertAsync(orderItem).ConfigureAwait(false);
            }
        }
    }
}
