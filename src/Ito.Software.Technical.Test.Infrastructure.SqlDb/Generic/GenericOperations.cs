﻿using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq.Expressions;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic
{
    public abstract class GenericOperations<TEntity> : IGenericOperations<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _entity;
        private readonly ItoSoftwareDbContext _itoSoftwareDbContext;

        public GenericOperations(ItoSoftwareDbContext itoSoftwareDbContext)
        {
            _entity = itoSoftwareDbContext.Set<TEntity>();
            _itoSoftwareDbContext = itoSoftwareDbContext;
        }

        public virtual IEnumerable<TEntity> Get(
    Expression<Func<TEntity, bool>> filter = null,
    Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    string includeProperties = "")
        {
            IQueryable<TEntity> query = _entity;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual async Task<TEntity> GetByIDAsync(object id)
        {
            return await _entity.FindAsync(id).ConfigureAwait(false);
        }


        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            EntityEntry<TEntity> entityEntry =  await _entity.AddAsync(entity).ConfigureAwait(false);
            return entityEntry.Entity;
        }

        public virtual async Task DeleteAsync(object id)
        {
            TEntity entityToDelete = await _entity.FindAsync(id).ConfigureAwait(false);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (_itoSoftwareDbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                _entity.Attach(entityToDelete);
            }
            _entity.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            _entity.Attach(entityToUpdate);
            _itoSoftwareDbContext.Entry(entityToUpdate).State = EntityState.Modified;
        }


    }
}
