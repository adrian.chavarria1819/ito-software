﻿using System.Linq.Expressions;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic.Interfaces
{
    public interface IGenericOperations<TEntity> where TEntity : class
    {
        void Delete(TEntity entityToDelete);
        Task DeleteAsync(object id);
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");
        Task<TEntity> GetByIDAsync(object id);
        //Task InsertAsync(TEntity entity);
        Task<TEntity> InsertAsync(TEntity entity);
        void Update(TEntity entityToUpdate);
    }
}