﻿using Ito.Software.Technical.Test.Domain.Models.Product;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Product.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Product
{
    public class ProductRepository : GenericOperations<ProductModel>, IProductRepository, IDisposable
    {
        private readonly ItoSoftwareDbContext _itoSoftwareDbContext;

        public ProductRepository(ItoSoftwareDbContext itoSoftwareDbContext) : base(itoSoftwareDbContext)
        {
            _itoSoftwareDbContext = itoSoftwareDbContext;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _itoSoftwareDbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
