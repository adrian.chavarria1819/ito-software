﻿using Ito.Software.Technical.Test.Domain.Models.Product;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Product.Interfaces
{
    public interface IProductRepository : IGenericOperations<ProductModel>, IDisposable
    {
    }
}
