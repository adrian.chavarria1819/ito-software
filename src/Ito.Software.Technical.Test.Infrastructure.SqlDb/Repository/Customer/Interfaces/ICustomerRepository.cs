﻿using Ito.Software.Technical.Test.Domain.Models.Customer;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer.Interfaces
{
    public interface ICustomerRepository : IGenericOperations<CustomerModel>
    {

    }
}