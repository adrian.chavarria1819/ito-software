﻿using Ito.Software.Technical.Test.Domain.Models.Customer;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer
{
    public class CustomerRepository : GenericOperations<CustomerModel>, ICustomerRepository, IDisposable
    {
        private readonly ItoSoftwareDbContext _itoSoftwareDbContext;

        public CustomerRepository(ItoSoftwareDbContext itoSoftwareDbContext) : base(itoSoftwareDbContext)
        {
            _itoSoftwareDbContext = itoSoftwareDbContext;
        }






        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _itoSoftwareDbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
