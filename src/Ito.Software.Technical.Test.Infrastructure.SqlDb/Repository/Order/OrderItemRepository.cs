﻿using Ito.Software.Technical.Test.Domain.Models.Order;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order.Interface;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order
{
    public class OrderItemRepository : GenericOperations<OrderItemModel>, IOrderItemRepository, IDisposable
    {
        private readonly ItoSoftwareDbContext _itoSoftwareDbContext;

        public OrderItemRepository(ItoSoftwareDbContext itoSoftwareDbContext) : base(itoSoftwareDbContext)
        {
            _itoSoftwareDbContext = itoSoftwareDbContext;
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _itoSoftwareDbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
