﻿using Ito.Software.Technical.Test.Domain.Models.Order;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order.Interface;
using System;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order
{
    public class OrderRepository : GenericOperations<OrderModel>, IOrderRepository, IDisposable
    {
        private bool disposed = false;
        private readonly ItoSoftwareDbContext _itoSoftwareDbContext;
        public OrderRepository(ItoSoftwareDbContext itoSoftwareDbContext) : base(itoSoftwareDbContext)
        {
            _itoSoftwareDbContext = itoSoftwareDbContext;
        }

       

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _itoSoftwareDbContext.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
