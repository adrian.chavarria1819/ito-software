﻿using Ito.Software.Technical.Test.Domain.Models.Order;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Generic.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order.Interface
{
    public interface IOrderItemRepository : IGenericOperations<OrderItemModel>, IDisposable
    {
    }
}
