﻿using Ito.Software.Technical.Test.Domain.Models.Customer;
using Ito.Software.Technical.Test.Domain.Models.Order;
using Ito.Software.Technical.Test.Domain.Models.Product;
using Ito.Software.Technical.Test.Domain.Models.Supplier;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Customer;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Order;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Product;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Supplier;
using Microsoft.EntityFrameworkCore;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Context
{
    public class ItoSoftwareDbContext : DbContext
    {
        public ItoSoftwareDbContext(DbContextOptions<ItoSoftwareDbContext> options) : base(options)
        {

        }

        public ItoSoftwareDbContext()
        {

        }

        protected override  void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderItemConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new SupplierConfiguration());
        }


        public DbSet<CustomerModel> Customers { get; set; }
        public DbSet<OrderModel> Orders { get; set; }
        public DbSet<OrderItemModel> OrderItems { get; set; }
        public DbSet<ProductModel> Products { get; set; }
        public DbSet<SupplierModel> Suppliers { get; set; }

    }
}
