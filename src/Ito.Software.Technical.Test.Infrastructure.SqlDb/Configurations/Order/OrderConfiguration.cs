﻿using Ito.Software.Technical.Test.Domain.Models.Order;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Order
{
    public class OrderConfiguration : IEntityTypeConfiguration<OrderModel>
    {
        public void Configure(EntityTypeBuilder<OrderModel> builder)
        {

            builder.HasKey(o => o.OrderId);
            builder.Property(o => o.OrderId).ValueGeneratedOnAdd();

            builder.Property(o => o.OrderNumber).HasMaxLength(10);

            builder.Property(o => o.TotalAmount).HasPrecision(12, 2);

            //keys
            builder.HasOne(o => o.Customer).WithMany(o => o.Orders);
            builder.HasMany(o => o.OrderItems).WithOne(o => o.Order);



        }
    }
}
