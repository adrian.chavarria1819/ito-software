﻿using Ito.Software.Technical.Test.Domain.Models.Order;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Order
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItemModel>
    {
        public void Configure(EntityTypeBuilder<OrderItemModel> builder)
        {
            builder.HasKey(oi => new { oi.OrderId, oi.ProductId });

            builder.Property(oi => oi.UnitPrice).HasPrecision(12, 2);

            builder.HasOne(o => o.Order).WithMany(o => o.OrderItems);
            builder.HasOne(o => o.Product).WithMany(o => o.OrderItems);

        }
    }
}
