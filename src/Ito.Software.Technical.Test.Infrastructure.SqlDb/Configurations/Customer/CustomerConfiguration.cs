﻿using Ito.Software.Technical.Test.Domain.Models.Customer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Customer
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<CustomerModel>
    {
        public void Configure(EntityTypeBuilder<CustomerModel> builder)
        {
            builder.HasKey(c => c.CustomerId);
            builder.Property(c => c.CustomerId).ValueGeneratedOnAdd();

            builder.HasAlternateKey(c => c.CustomerName);
            builder.Property(c => c.CustomerName).HasMaxLength(40);

            builder.Property(c => c.Phone).HasMaxLength(20);

            builder.HasMany(c => c.Orders).WithOne(c => c.Customer);
        }
    }
}
