﻿using Ito.Software.Technical.Test.Domain.Models.Supplier;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Supplier
{
    public class SupplierConfiguration : IEntityTypeConfiguration<SupplierModel>
    {
        public void Configure(EntityTypeBuilder<SupplierModel> builder)
        {
            builder.HasKey(s => s.SupplierId);
            builder.Property(s => s.SupplierId).ValueGeneratedOnAdd();

            builder.Property(s => s.CompanyName).HasMaxLength(40);
            builder.HasAlternateKey(s => s.CompanyName);

            builder.Property(s => s.Phone).HasMaxLength(20);

            builder.HasMany(s => s.Products).WithOne(s => s.Supplier);
        }
    }
}
