﻿using Ito.Software.Technical.Test.Domain.Models.Product;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Configurations.Product
{
    public class ProductConfiguration : IEntityTypeConfiguration<ProductModel>
    {
        public void Configure(EntityTypeBuilder<ProductModel> builder)
        {
            builder.HasKey(p => p.ProductId);
            builder.Property(p => p.ProductId).ValueGeneratedOnAdd();

            builder.Property(p => p.ProductName).HasMaxLength(50);
            builder.HasAlternateKey(p => p.ProductName);
            
            builder.HasAlternateKey(p => new { p.SupplierId, p.ProductName});
            builder.HasOne(p => p.Supplier).WithMany(p => p.Products).HasForeignKey(o => o.SupplierId).IsRequired(false);
            builder.Property(p => p.SupplierId).HasDefaultValue(null);

            builder.Property(p => p.UnitPrice).HasPrecision(12, 2);


        }
    }
}
