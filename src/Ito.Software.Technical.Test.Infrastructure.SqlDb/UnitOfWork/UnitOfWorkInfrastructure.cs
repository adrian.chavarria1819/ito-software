﻿using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer.Interfaces;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order.Interface;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Product;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Product.Interfaces;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork
{
    public class UnitOfWorkInfrastructure : IUnitOfWorkInfrastructure, IDisposable
    {
        private bool disposed = false;

        private readonly ItoSoftwareDbContext _itoSoftwareDbContext;
        private ICustomerRepository? _customerRepository;
        private IProductRepository? _productRepository;
        private IOrderRepository? _orderRepository;
        private IOrderItemRepository? _orderItemRepository;


        public UnitOfWorkInfrastructure(ItoSoftwareDbContext itoSoftwareDbContext)
        {
            _itoSoftwareDbContext = itoSoftwareDbContext;
        }

        public ICustomerRepository CustomerRepository
        {
            get
            {
                if (_customerRepository == null)
                    _customerRepository = new CustomerRepository(_itoSoftwareDbContext);
                return _customerRepository;
            }
        }

        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                    _productRepository = new ProductRepository(_itoSoftwareDbContext);
                return _productRepository;
            }
        }

        public IOrderRepository OrderRepository
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepository(_itoSoftwareDbContext);
                return _orderRepository;
            }
        }

        public IOrderItemRepository OrderItemRepository
        {
            get
            {
                if (_orderItemRepository == null)
                    _orderItemRepository = new OrderItemRepository(_itoSoftwareDbContext);
                return _orderItemRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _itoSoftwareDbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _itoSoftwareDbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
