﻿using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer.Interfaces;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Order.Interface;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Product.Interfaces;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork.Interfaces
{
    public interface IUnitOfWorkInfrastructure : IDisposable
    {
        public ICustomerRepository CustomerRepository { get; }
        public IProductRepository ProductRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderItemRepository OrderItemRepository { get; }
        Task SaveAsync();
    }
}