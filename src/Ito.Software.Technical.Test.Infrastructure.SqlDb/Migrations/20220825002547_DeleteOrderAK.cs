﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Migrations
{
    public partial class DeleteOrderAK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Orders_OrderNumber",
                table: "Orders");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddUniqueConstraint(
                name: "AK_Orders_OrderNumber",
                table: "Orders",
                column: "OrderNumber");
        }
    }
}
