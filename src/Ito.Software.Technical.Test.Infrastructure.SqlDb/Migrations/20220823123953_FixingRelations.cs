﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Migrations
{
    public partial class FixingRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Suppliers_SupplierTempId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_SupplierTempId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SupplierTempId",
                table: "Products");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Suppliers_SupplierId",
                table: "Products",
                column: "SupplierId",
                principalTable: "Suppliers",
                principalColumn: "SupplierId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Suppliers_SupplierId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "SupplierTempId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Products_SupplierTempId",
                table: "Products",
                column: "SupplierTempId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Suppliers_SupplierTempId",
                table: "Products",
                column: "SupplierTempId",
                principalTable: "Suppliers",
                principalColumn: "SupplierId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
