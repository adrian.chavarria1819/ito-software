﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb.Migrations
{
    public partial class FixingProductAK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Products_SupplierId",
                table: "Products");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Products_SupplierId_ProductName",
                table: "Products",
                columns: new[] { "SupplierId", "ProductName" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Products_SupplierId_ProductName",
                table: "Products");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Products_SupplierId",
                table: "Products",
                column: "SupplierId");
        }
    }
}
