﻿using Ito.Software.Technical.Test.Infrastructure.SqlDb.Context;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.Repository.Customer.Interfaces;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork;
using Ito.Software.Technical.Test.Infrastructure.SqlDb.UnitOfWork.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ito.Software.Technical.Test.Infrastructure.SqlDb
{
    public static class InfrastructureSqlDbExtension
    {

        public static void AddInfrastructureSqlDb(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddDbContext<ItoSoftwareDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("default"))
            );

            service.AddRepositories();
            service.AddUnitOfWork();
        }


        private static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<ICustomerRepository, CustomerRepository>();
        }
        private static void AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWorkInfrastructure, UnitOfWorkInfrastructure>();
        }
    }
}
