﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Ito.Software.Technical.Test.App.Client.Components
{
    public partial class ModalComponent
    {
        [Parameter]
        public string ButtonText { get; set; } = string.Empty;

        [Parameter]
        public string ModalTitle { get; set; } = string.Empty;

        [Parameter]
        public string CloseButtonText { get; set; } = "Close";

        [Parameter]
        public string OkButtonText { get; set; } = "Ok";

        [Parameter]
        public bool IsOkButtonSubmitType { get; set; } = false;

        [Parameter]
        public Guid FormId { get; set; } = Guid.Empty;

        [Parameter]
        public RenderFragment? ChildContent { get; set; }

    }
}
