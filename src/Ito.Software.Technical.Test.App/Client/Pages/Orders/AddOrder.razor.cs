﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces;
using Ito.Software.Technical.Test.App.Client.ViewModel.Order.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using Ito.Software.Technical.Test.Domain.Dtos.Product;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Ito.Software.Technical.Test.App.Client.Pages.Orders
{
    public partial class AddOrder
    {
        [Inject]
        private IOrderViewModel _OrderViewModel { get; set; }

        [Inject]
        private IProductViewModel _ProductViewModel { get; set; }

        [Inject]
        private ILogger<AddOrder> _Logger { get; set; }

        [Parameter]
        public EventCallback<OrderDto> OnSaveData { get; set; }


        public OrderDto OrderDto { get; set; } = new();

        public IList<CustomerDto> Customers { get; set; } = new List<CustomerDto>();
        public IList<ProductDto> Products { get; set; } = new List<ProductDto>();

        public ProductDto ProductSelected { get; set; } = new();

        public int Qty { get; set; }




        public Guid AddFormId { get; set; } = Guid.NewGuid();

        public void HandleValidSubmit()
        {
            OnSaveData.InvokeAsync(OrderDto).ConfigureAwait(false);
        }

        protected override async Task OnInitializedAsync()
        {
            Customers = await _OrderViewModel.GetCustomerAsync().ConfigureAwait(false);
            Products = await _ProductViewModel.GetAsync().ConfigureAwait(false);
        }

        public void OnProductChanged(ChangeEventArgs e)
        {
            string id = e.Value?.ToString() ?? string.Empty;
            ProductSelected = Products.FirstOrDefault(p => p.Id == int.Parse(id)) ?? new();
            _Logger.LogInformation($"Product has changed! {id}");
        }

        public void AddToOrder()
        {
            OrderDto.OrderItems.Add(
                new OrderItemDto
                {
                    ProductId = ProductSelected.Id,
                    ProductName = ProductSelected.Name,
                    UnitPrice = ProductSelected.UnitPrice,
                    Quantity = Qty
                });
            OrderDto.TotalAmount = OrderDto.OrderItems.Sum(oi => (oi.Quantity * oi.UnitPrice));
        }


    }
}
