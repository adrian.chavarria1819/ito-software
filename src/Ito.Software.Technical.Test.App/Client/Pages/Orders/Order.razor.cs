﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Order.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using Microsoft.AspNetCore.Components;

namespace Ito.Software.Technical.Test.App.Client.Pages.Orders
{
    public partial class Order
    {
        public IList<OrderDto> Orders { get; set; } = new List<OrderDto>();
        
        [Inject]
        private ILogger<Order>? _Logger { get; set; }
        
        [Inject]
        private IOrderViewModel _OrderViewModel { get; set; }


        protected override async Task OnInitializedAsync()
        {
             Orders= await _OrderViewModel.GetAsync().ConfigureAwait(false);
        }


        private async Task<bool> HandleValidSubmitAsync(OrderDto order)
        {
            bool isCreated = await _OrderViewModel!.CreateAsync(order).ConfigureAwait(false);
            if (isCreated)
                _Logger!.LogInformation("Order created!!!");
            return isCreated;
        }
    }
}
