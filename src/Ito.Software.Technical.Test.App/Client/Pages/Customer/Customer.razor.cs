﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;

namespace Ito.Software.Technical.Test.App.Client.Pages.Customer
{
    public partial class Customer
    {

        [Inject]
        private ILogger<Customer>? _Logger { get; set; }

        [Inject]
        private ICustomerViewModel? _CustomerViewModel { get; set; }

        protected IList<CustomerDto> Customers { get; set; }


        private async Task HandleValidSubmitAsync(CustomerDto customer)
        {
            _Logger!.LogInformation("It is a valid from data");
            bool isCreated = await _CustomerViewModel!.CreateAsync(customer).ConfigureAwait(false);
            if (isCreated)
                _Logger!.LogInformation("Customer created!!!");
        }


        protected override async Task OnInitializedAsync()
        {
            Customers = await _CustomerViewModel!.GetAsync().ConfigureAwait(false);
        }



    }
}
