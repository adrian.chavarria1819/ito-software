﻿using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Ito.Software.Technical.Test.App.Client.Pages.Customer
{
    public partial class AddCustomer
    {
        public CustomerDto CustomerDto { get; set; } = new();

        protected Guid AddFormId { get; set; } = Guid.NewGuid();

        [Parameter]
        public EventCallback<CustomerDto> OnSaveData { get; set; }

        public void HandleValidSubmit()
        {
            OnSaveData.InvokeAsync(CustomerDto);
        }
    }
}
