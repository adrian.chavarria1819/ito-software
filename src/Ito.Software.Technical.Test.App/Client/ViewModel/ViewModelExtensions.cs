﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Customer;
using Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces;
using Ito.Software.Technical.Test.App.Client.ViewModel.Order;
using Ito.Software.Technical.Test.App.Client.ViewModel.Order.Interfaces;
using Ito.Software.Technical.Test.App.Client.ViewModel.Product;

namespace Ito.Software.Technical.Test.App.Client.ViewModel
{
    public static class ViewModelExtensions
    {
        public static void AddViewModel(this IServiceCollection service)
        {
            service.AddTransient<ICustomerViewModel, CustomerViewModel>();
            service.AddTransient<IOrderViewModel, OrderViewModel>();
            service.AddTransient<IProductViewModel, ProductViewModel>();
        }
    }
}
