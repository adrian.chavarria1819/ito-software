﻿using Ito.Software.Technical.Test.Domain.Dtos.Customer;

namespace Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces
{
    public interface ICustomerViewModel
    {
        Task<bool> CreateAsync(CustomerDto customerDto);
        Task<IList<CustomerDto>> GetAsync();
    }
}