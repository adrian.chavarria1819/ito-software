﻿using Ito.Software.Technical.Test.Domain.Dtos.Product;

namespace Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces
{
    public interface IProductViewModel
    {
        Task<IList<ProductDto>> GetAsync();
    }
}