﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;

namespace Ito.Software.Technical.Test.App.Client.ViewModel.Customer
{
    public class CustomerViewModel : ICustomerViewModel
    {
        private readonly HttpClient _httpClient;

        public CustomerViewModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<bool> CreateAsync(CustomerDto customerDto)
        {
            try
            {
                StringContent stringContent = new(JsonSerializer.Serialize(customerDto), Encoding.UTF8, "application/json");
                HttpResponseMessage reponse = await _httpClient.PostAsync("Customer", stringContent).ConfigureAwait(false);
                reponse.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public async Task<IList<CustomerDto>> GetAsync()
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<IList<CustomerDto>>("Customer").ConfigureAwait(false);
            }
            catch (Exception)
            {
                return new List<CustomerDto>();

            }
        }

    }
}
