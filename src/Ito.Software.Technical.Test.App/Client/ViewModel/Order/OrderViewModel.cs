﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces;
using Ito.Software.Technical.Test.App.Client.ViewModel.Order.Interfaces;
using Ito.Software.Technical.Test.Business.Customer.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;

namespace Ito.Software.Technical.Test.App.Client.ViewModel.Order
{
    public class OrderViewModel : IOrderViewModel
    {
        private readonly ICustomerViewModel _customerViewModel;
        private readonly HttpClient _httpClient;

        public OrderViewModel(ICustomerViewModel customerViewModel, HttpClient httpClient)
        {
            _customerViewModel = customerViewModel;
            _httpClient = httpClient;
        }

        public async Task<IList<CustomerDto>> GetCustomerAsync()
        {
            return await _customerViewModel.GetAsync().ConfigureAwait(false);
        }

        public async Task<bool> CreateAsync(OrderDto orderDto)
        {
            try
            {
                StringContent stringContent = new(JsonSerializer.Serialize(orderDto), Encoding.UTF8, "application/json");
                HttpResponseMessage reponse = await _httpClient.PostAsync("Order", stringContent).ConfigureAwait(false);
                reponse.EnsureSuccessStatusCode();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public async Task<IList<OrderDto>> GetAsync()
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<IList<OrderDto>>("Order").ConfigureAwait(false);
            }
            catch (Exception)
            {
                return new List<OrderDto>();
            }
        }

    }
}
