﻿using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Ito.Software.Technical.Test.Domain.Dtos.Order;

namespace Ito.Software.Technical.Test.App.Client.ViewModel.Order.Interfaces
{
    public interface IOrderViewModel
    {
        Task<IList<CustomerDto>> GetCustomerAsync();
        Task<bool> CreateAsync(OrderDto orderDto);
        Task<IList<OrderDto>> GetAsync();
    }
}