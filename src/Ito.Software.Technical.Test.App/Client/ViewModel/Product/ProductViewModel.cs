﻿using Ito.Software.Technical.Test.App.Client.ViewModel.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Product;
using System.Net.Http.Json;

namespace Ito.Software.Technical.Test.App.Client.ViewModel.Product
{
    public class ProductViewModel : IProductViewModel
    {
        private readonly HttpClient _httpClient;

        public ProductViewModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IList<ProductDto>> GetAsync()
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<IList<ProductDto>>("Product").ConfigureAwait(false);
            }
            catch (Exception)
            {
                return new List<ProductDto>();

            }
        }
    }
}
