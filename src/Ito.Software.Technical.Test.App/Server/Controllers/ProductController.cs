﻿using Ito.Software.Technical.Test.Business.Product.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Product;
using Microsoft.AspNetCore.Mvc;

namespace Ito.Software.Technical.Test.App.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductBusiness _productBusiness;
        private readonly ILogger<ProductController> _logger;
        public ProductController(IProductBusiness productBusiness, ILogger<ProductController> logger)
        {
            _productBusiness = productBusiness;
            _logger = logger;
        }

        [HttpGet]
        public IList<ProductDto> Get()
        {
            try
            {
                return _productBusiness.Get();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return new List<ProductDto>();
            }
        }
    }
}
