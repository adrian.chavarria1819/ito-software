﻿using Ito.Software.Technical.Test.Business.Customer.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Customer;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Ito.Software.Technical.Test.App.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerBusiness _customerBusiness;
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(ICustomerBusiness customerBusiness, ILogger<CustomerController> logger)
        {
            _customerBusiness = customerBusiness;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CustomerDto customerDto)
        {
            try
            {
                await _customerBusiness.AddAsync(customerDto).ConfigureAwait(false);
                return StatusCode((int) HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest();
            }

        }


        [HttpGet]
        public IList<CustomerDto> Get()
        {
            try
            {
                return _customerBusiness.Get();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new List<CustomerDto>();
            }

        }
    }
}
