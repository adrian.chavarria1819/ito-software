﻿using Ito.Software.Technical.Test.Business.Order.Interfaces;
using Ito.Software.Technical.Test.Domain.Dtos.Order;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Ito.Software.Technical.Test.App.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {

        private readonly IOrderBusiness _orderBusiness;
        private readonly ILogger<OrderController> _logger;

        public OrderController(IOrderBusiness orderBusiness, ILogger<OrderController> logger)
        {
            _orderBusiness = orderBusiness;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] OrderDto order)
        {
            try
            {
                await _orderBusiness.CreateAsync(order).ConfigureAwait(false);
                return StatusCode((int)HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<IList<OrderDto>> GetAsync()
        {
            try
            {
                return await _orderBusiness.GetAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return new List<OrderDto>();
            }
            
        }
    }
}
