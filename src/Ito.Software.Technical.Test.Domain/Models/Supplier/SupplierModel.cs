﻿using Ito.Software.Technical.Test.Domain.Models.Product;

namespace Ito.Software.Technical.Test.Domain.Models.Supplier
{
    public class SupplierModel
    {
        public int SupplierId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }

        //Relations
        public virtual ICollection<ProductModel> Products { get; set; }
    }
}
