﻿using Ito.Software.Technical.Test.Domain.Models.Order;
using Ito.Software.Technical.Test.Domain.Models.Supplier;

namespace Ito.Software.Technical.Test.Domain.Models.Product
{
    public class ProductModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SupplierId { get; set; }
        public double? UnitPrice { get; set; }
        public bool IsDiscontinued { get; set; } = false;


        //Relations
        public virtual SupplierModel? Supplier { get; set; }
        public virtual ICollection<OrderItemModel> OrderItems { get; set; }



    }
}
