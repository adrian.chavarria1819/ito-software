﻿using Ito.Software.Technical.Test.Domain.Models.Customer;

namespace Ito.Software.Technical.Test.Domain.Models.Order
{
    public class OrderModel
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; } = string.Empty;
        public int CustomerId { get; set; }
        public DateTime OrderDate { get; set; }
        public double TotalAmount { get; set; }



        //Relations
        public virtual CustomerModel Customer { get; set; }

        public virtual ICollection<OrderItemModel> OrderItems { get; set; }

    }
}
