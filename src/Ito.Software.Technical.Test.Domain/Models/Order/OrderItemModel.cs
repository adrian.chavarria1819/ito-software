﻿using Ito.Software.Technical.Test.Domain.Models.Product;

namespace Ito.Software.Technical.Test.Domain.Models.Order
{
    public class OrderItemModel
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }

        //TODO: only two decimals!!
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }

        //relations
        public virtual OrderModel Order { get; set; }
        public virtual ProductModel Product { get; set; }
    }
}
