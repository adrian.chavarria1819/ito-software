﻿using Ito.Software.Technical.Test.Domain.Models.Order;

namespace Ito.Software.Technical.Test.Domain.Models.Customer
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string? Phone { get; set; }
            
        //Relations
        public virtual ICollection<OrderModel>? Orders { get; set; }
    }
}
