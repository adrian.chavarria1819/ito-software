﻿using System.ComponentModel.DataAnnotations;

namespace Ito.Software.Technical.Test.Domain.Dtos.Customer
{
    public class CustomerDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(40, ErrorMessage = "Name is too long.")]
        public string? Name { get; set; }

        [StringLength(20, ErrorMessage = "Phone is too long.")]
        public string? Phone { get; set; }
    }
}
