﻿using System.ComponentModel.DataAnnotations;

namespace Ito.Software.Technical.Test.Domain.Dtos.Order
{
    public class OrderItemDto
    {
        [Required]
        public int OrderId { get; set; }
       
        [Required]
        public int ProductId { get; set; }
        
        [Required]
        public double UnitPrice { get; set; }
        
        [Required]
        public int Quantity { get; set; }

        public string ProductName { get; set; }
    }
}
