﻿using System.ComponentModel.DataAnnotations;

namespace Ito.Software.Technical.Test.Domain.Dtos.Order
{
    public class OrderDto
    {
        public int OrderId { get; set; }
        [Required]
        public string OrderNumber { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public DateTime OrderDate { get; set; } = DateTime.UtcNow;

        [Required]
        public double TotalAmount { get; set; }
        public IList<OrderItemDto> OrderItems { get; set; } = new List<OrderItemDto>();

    }
}
