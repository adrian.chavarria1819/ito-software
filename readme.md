# ¿cómo ejecutar las migraciones?


En la raiz del proyecto ejecutar el comando

``` 
dotnet ef database update \
-s .\src\Ito.Software.Technical.Test.App\Server\Ito.Software.Technical.Test.App.Server.csproj \
-p .\src\Ito.Software.Technical.Test.Infrastructure.SqlDb\ \
```


# Configuracion del string de conexión


En el archivo appsetting.json o appsetting.Development.json (para desarrollo) agregar el siguiente nodo


```
  "ConnectionStrings": {
    "Default": "[YOUR CONNECTION STRING HERE]"
  }
```

# Arquitectura del proyecto

## Backend
* Architecture onion


## Frontend
* Architecture Model-view-viewModel

# Patrones
 * Unit of work
 * Repository
 

# Requerimientos

 - [X] Razor page  
 - [X] Generar order de una lista de productos
 - [X] Modales
 - [X] Botón de checkout que permita direccionar a una página de pagos
 - [X] Modelamiento de base de datos (Code first)
 - [X] Construcción de elementos dinámicos (Companentes)
 - [ ] Uso de Jquery
 - [X] Uso de bootstrap
 
 
 # _TODOS:_
 
 * Validaciones a nivel de frontend
 * Más logs de trazabilidad de los procesos
 * Pruebas unitarias
 * Mejora de experiencia de usuario con los modales y en general
 
 
 
 # Total tiempo desarrollo
 5 horas
 



